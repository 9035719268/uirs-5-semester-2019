#ifndef VAR58_H
#define VAR58_H

#include "IVar.h"

// Variant 58 - Heading and Pitch [ha]
class Var58 : public IVar
{
private:
    u1 allValuesArr[15]{};
    u1 checkSum;
    f4 headingValue_1;
    f4 headingValue_2;
    f4 headingValue_3;
    f4 headingValue_4;
    f4 pitchValue_1;
    f4 pitchValue_2;
    f4 pitchValue_3;
    f4 pitchValue_4;
    u1 solTypeValue;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR58_H