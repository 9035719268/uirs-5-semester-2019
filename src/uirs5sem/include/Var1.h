#ifndef VAR1_H
#define VAR1_H

#include "IVar.h"

// Variant 1 - Receiver Time [~~]
class Var1 : public IVar
{
private:
    u1 allValuesArr[10]{};
    u1 checkSum;
    u4 todValue_1;
    u4 todValue_2;
    u4 todValue_3;
    u4 todValue_4;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR1_H