#ifndef VAR2_H
#define VAR2_H

#include "IVar.h"

// Variant 2 - Epoch Time [::]
class Var2 : public IVar
{
private:
    u1 allValuesArr[10]{};
    u1 checkSum;
    u4 todValue_1;
    u4 todValue_2;
    u4 todValue_3;
    u4 todValue_4;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR2_H