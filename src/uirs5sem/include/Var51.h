#ifndef VAR51_H
#define VAR51_H

#include "IVar.h"

// Variant 51 - Rotation Angles [AR]
class Var51 : public IVar
{
private:
    u1 allValuesArr[38]{};
    u1 checkSum;
    u4 timeValue_1;
    u4 timeValue_2;
    u4 timeValue_3;
    u4 timeValue_4;
    f4 pValue_1;
    f4 pValue_2;
    f4 pValue_3;
    f4 pValue_4;
    f4 rValue_1;
    f4 rValue_2;
    f4 rValue_3;
    f4 rValue_4;
    f4 hValue_1;
    f4 hValue_2;
    f4 hValue_3;
    f4 hValue_4;
    f4 spValue_1;
    f4 spValue_2;
    f4 spValue_3;
    f4 spValue_4;
    f4 srValue_1;
    f4 srValue_2;
    f4 srValue_3;
    f4 srValue_4;
    f4 shValue_1;
    f4 shValue_2;
    f4 shValue_3;
    f4 shValue_4;
    u1 solTypeValue_1;
    u1 solTypeValue_2;
    u1 solTypeValue_3;
    u1 flagsValue;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR51_H