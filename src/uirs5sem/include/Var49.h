#ifndef VAR49_H
#define VAR49_H

#include "IVar.h"

// Variant 49 - Rotation Matrix [MR]
class Var49 : public IVar
{
private:
    u1 allValuesArr[42]{};
    u1 checkSum;
    u4 timeValue_1;
    u4 timeValue_2;
    u4 timeValue_3;
    u4 timeValue_4;
    f4 q00Value_1;
    f4 q00Value_2;
    f4 q00Value_3;
    f4 q00Value_4;
    f4 q01Value_1;
    f4 q01Value_2;
    f4 q01Value_3;
    f4 q01Value_4;
    f4 q02Value_1;
    f4 q02Value_2;
    f4 q02Value_3;
    f4 q02Value_4;
    f4 q12Value_1;
    f4 q12Value_2;
    f4 q12Value_3;
    f4 q12Value_4;
    f4 rmsValue_1;
    f4 rmsValue_2;
    f4 rmsValue_3;
    f4 rmsValue_4;
    f4 rmsValue_5;
    f4 rmsValue_6;
    f4 rmsValue_7;
    f4 rmsValue_8;
    f4 rmsValue_9;
    f4 rmsValue_10;
    f4 rmsValue_11;
    f4 rmsValue_12;
    u1 solTypeValue_1;
    u1 solTypeValue_2;
    u1 solTypeValue_3;
    u1 flagValue;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR49_H