#ifndef VAR68_H
#define VAR68_H

#include "IVar.h"

// Variant 68 - Epoch End [||]
class Var68 : public IVar
{
private:
    u1 allValuesArr[6]{};
    u1 checkSum;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR68_H