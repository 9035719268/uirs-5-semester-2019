#ifndef VAR56_H
#define VAR56_H

#include "IVar.h"

// Variant 56 - PPS Offset [ZA], [ZB]
class Var56 : public IVar
{
private:
    u1 allValuesArr[10]{};
    u1 checkSum;
    f4 offsValue_1;
    f4 offsValue_2;
    f4 offsValue_3;
    f4 offsValue_4;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR56_H