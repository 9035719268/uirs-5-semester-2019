#ifndef VAR26_H
#define VAR26_H

#include "IVar.h"

// Variant 26 - Cartesian Velocity [VE]
class Var26 : public IVar
{
private:
    u1 allValuesArr[23]{};
    u1 checkSum;
    f4 xValue_1;
    f4 xValue_2;
    f4 xValue_3;
    f4 xValue_4;
    f4 yValue_1;
    f4 yValue_2;
    f4 yValue_3;
    f4 yValue_4;
    f4 zValue_1;
    f4 zValue_2;
    f4 zValue_3;
    f4 zValue_4;
    f4 vSigmaValue_1;
    f4 vSigmaValue_2;
    f4 vSigmaValue_3;
    f4 vSigmaValue_4;
    u1 solTypeValue;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR26_H