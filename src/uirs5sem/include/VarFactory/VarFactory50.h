#ifndef VARFACTORY50_H
#define VARFACTORY50_H

#include "IVarFactory.h"
#include "../Var50.h"

class VarFactory50 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY50_H