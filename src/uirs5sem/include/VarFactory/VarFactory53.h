#ifndef VARFACTORY53_H
#define VARFACTORY53_H

#include "IVarFactory.h"
#include "../Var53.h"

class VarFactory53 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY53_H