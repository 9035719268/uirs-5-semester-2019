#ifndef VARFACTORY32_H
#define VARFACTORY32_H

#include "IVarFactory.h"
#include "../Var32.h"

class VarFactory32 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY32_H