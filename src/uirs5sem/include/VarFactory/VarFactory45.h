#ifndef VARFACTORY45_H
#define VARFACTORY45_H

#include "IVarFactory.h"
#include "../Var45.h"

class VarFactory45 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY45_H