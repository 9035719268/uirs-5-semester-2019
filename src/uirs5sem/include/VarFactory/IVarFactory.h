#ifndef IVARFACTORY_H
#define IVARFACTORY_H

#include "../IVar.h"

class IVarFactory
{
public:
    virtual IVar* createVar() = 0;
    virtual ~IVarFactory() {}
};

#endif // !IVARFACTORY_H