#ifndef VARFACTORY64_H
#define VARFACTORY64_H

#include "IVarFactory.h"
#include "../Var64.h"

class VarFactory64 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY64_H