#ifndef VARFACTORY63_H
#define VARFACTORY63_H

#include "IVarFactory.h"
#include "../Var63.h"

class VarFactory63 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY63_H