#ifndef VARFACTORY49_H
#define VARFACTORY49_H

#include "IVarFactory.h"
#include "../Var49.h"

class VarFactory49 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY49_H