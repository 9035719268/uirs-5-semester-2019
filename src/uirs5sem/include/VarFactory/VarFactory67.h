#ifndef VARFACTORY67_H
#define VARFACTORY67_H

#include "IVarFactory.h"
#include "../Var67.h"

class VarFactory67 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY67_H