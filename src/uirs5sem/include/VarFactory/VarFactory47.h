#ifndef VARFACTORY47_H
#define VARFACTORY47_H

#include "IVarFactory.h"
#include "../Var47.h"

class VarFactory47 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY47_H