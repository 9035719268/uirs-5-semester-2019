#ifndef VARFACTORY20_H
#define VARFACTORY20_H

#include "IVarFactory.h"
#include "../Var20.h"

class VarFactory20 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY20_H