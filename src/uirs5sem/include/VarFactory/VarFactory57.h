#ifndef VARFACTORY57_H
#define VARFACTORY57_H

#include "IVarFactory.h"
#include "../Var57.h"

class VarFactory57 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY57_H