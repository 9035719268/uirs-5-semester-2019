#ifndef VARFACTORY39_H
#define VARFACTORY39_H

#include "IVarFactory.h"
#include "../Var39.h"

class VarFactory39 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY39_H