#ifndef VARFACTORY27_H
#define VARFACTORY27_H

#include "IVarFactory.h"
#include "../Var27.h"

class VarFactory27 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY27_H