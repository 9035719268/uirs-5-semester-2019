#ifndef VARFACTORY13_H
#define VARFACTORY13_H

#include "IVarFactory.h"
#include "../Var13.h"

class VarFactory13 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY13_H