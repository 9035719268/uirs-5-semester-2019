#ifndef VARFACTORY51_H
#define VARFACTORY51_H

#include "IVarFactory.h"
#include "../Var51.h"

class VarFactory51 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY51_H