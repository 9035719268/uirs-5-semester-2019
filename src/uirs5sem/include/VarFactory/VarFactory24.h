#ifndef VARFACTORY24_H
#define VARFACTORY24_H

#include "IVarFactory.h"
#include "../Var24.h"

class VarFactory24 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY24_H