#ifndef VARFACTORY15_H
#define VARFACTORY15_H

#include "IVarFactory.h"
#include "../Var15.h"

class VarFactory15 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY15_H