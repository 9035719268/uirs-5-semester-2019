#ifndef VARFACTORY46_H
#define VARFACTORY46_H

#include "IVarFactory.h"
#include "../Var46.h"

class VarFactory46 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY46_H