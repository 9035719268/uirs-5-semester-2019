#ifndef VARFACTORY34_H
#define VARFACTORY34_H

#include "IVarFactory.h"
#include "../Var34.h"

class VarFactory34 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY34_H