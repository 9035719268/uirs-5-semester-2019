#ifndef VARFACTORY35_H
#define VARFACTORY35_H

#include "IVarFactory.h"
#include "../Var35.h"

class VarFactory35 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY35_H