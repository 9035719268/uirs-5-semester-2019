#ifndef VARFACTORY58_H
#define VARFACTORY58_H

#include "IVarFactory.h"
#include "../Var58.h"

class VarFactory58 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY58_H