#ifndef VARFACTORY2_H
#define VARFACTORY2_H

#include "IVarFactory.h"
#include "../Var2.h"

class VarFactory2 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY2_H