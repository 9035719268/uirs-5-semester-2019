#ifndef VARFACTORY8_H
#define VARFACTORY8_H

#include "IVarFactory.h"
#include "../Var8.h"

class VarFactory8 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY8_H