#ifndef VARFACTORY33_H
#define VARFACTORY33_H

#include "IVarFactory.h"
#include "../Var33.h"

class VarFactory33 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY33_H