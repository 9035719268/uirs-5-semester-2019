#ifndef VAR66_H
#define VAR66_H

#include "IVar.h"

// Variant 66 - CA/L1 Overall Continuous Tracking Time [TT]
class Var66 : public IVar
{
private:
    u1 allValuesArr[10]{};
    u1 checkSum;
    u4 ttValue_1;
    u4 ttValue_2;
    u4 ttValue_3;
    u4 ttValue_4;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR66_H