#include <iostream>
#include <fstream>
#include "../include/Var54.h"

int Var54::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x5415D]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x5415E]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x5415F]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x54160]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x54161]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x54162]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x54163]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x54164]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x54165]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x54166]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x54167]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x54168]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x54169]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x5416A]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x5416B]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x5416C]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x5416D]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x5416E]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x5416F]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x54170]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x54171]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x54172]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x54173]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x54174]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x54175]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x54176]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x54177]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x54178]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x54179]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x5417A]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x5417B]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x5417C]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x5417D]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x5417E]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x5417F]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x54180]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x54181]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x54182]);
        this->allValuesArr[38] = static_cast<u1>(tempString[0x54183]);
        this->allValuesArr[39] = static_cast<u1>(tempString[0x54184]);
        this->allValuesArr[40] = static_cast<u1>(tempString[0x54185]);
        this->allValuesArr[41] = static_cast<u1>(tempString[0x54186]);
        this->allValuesArr[42] = static_cast<u1>(tempString[0x54187]);

        this->timeValue_1 = this->allValuesArr[5];
        this->timeValue_2 = this->allValuesArr[6];
        this->timeValue_3 = this->allValuesArr[7];
        this->timeValue_4 = this->allValuesArr[8];
        this->accelerationsValue_1 = this->allValuesArr[9];
        this->accelerationsValue_2 = this->allValuesArr[10];
        this->accelerationsValue_3 = this->allValuesArr[11];
        this->accelerationsValue_4 = this->allValuesArr[12];
        this->accelerationsValue_5 = this->allValuesArr[13];
        this->accelerationsValue_6 = this->allValuesArr[14];
        this->accelerationsValue_7 = this->allValuesArr[15];
        this->accelerationsValue_8 = this->allValuesArr[16];
        this->accelerationsValue_9 = this->allValuesArr[17];
        this->accelerationsValue_10 = this->allValuesArr[18];
        this->accelerationsValue_11 = this->allValuesArr[19];
        this->accelerationsValue_12 = this->allValuesArr[20];
        this->inductionValue_1 = this->allValuesArr[21];
        this->inductionValue_2 = this->allValuesArr[22];
        this->inductionValue_3 = this->allValuesArr[23];
        this->inductionValue_4 = this->allValuesArr[24];
        this->inductionValue_5 = this->allValuesArr[25];
        this->inductionValue_6 = this->allValuesArr[26];
        this->inductionValue_7 = this->allValuesArr[27];
        this->inductionValue_8 = this->allValuesArr[28];
        this->inductionValue_9 = this->allValuesArr[29];
        this->inductionValue_10 = this->allValuesArr[30];
        this->inductionValue_11 = this->allValuesArr[31];
        this->inductionValue_12 = this->allValuesArr[32];
        this->magnitudeValue_1 = this->allValuesArr[33];
        this->magnitudeValue_2 = this->allValuesArr[34];
        this->magnitudeValue_3 = this->allValuesArr[35];
        this->magnitudeValue_4 = this->allValuesArr[36];
        this->temperatureValue_1 = this->allValuesArr[37];
        this->temperatureValue_2 = this->allValuesArr[38];
        this->temperatureValue_3 = this->allValuesArr[39];
        this->temperatureValue_4 = this->allValuesArr[40];
        this->calibratedValue = this->allValuesArr[41];
        this->csValue = this->allValuesArr[42];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var54::handleCS()
{
    this->checkSum = cs(allValuesArr, 42);
}

void Var54::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var54_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 54" << std::endl;
    outputFile << "Structure AccMag" << std::endl;
    outputFile << "Receiver time: "
        << static_cast<int>(timeValue_1) << " "
        << static_cast<int>(timeValue_2) << " "
        << static_cast<int>(timeValue_3) << " "
        << static_cast<int>(timeValue_4) << std::endl;
    outputFile << "Ax: "
        << static_cast<int>(accelerationsValue_1) << " "
        << static_cast<int>(accelerationsValue_2) << " "
        << static_cast<int>(accelerationsValue_3) << " "
        << static_cast<int>(accelerationsValue_4) << std::endl;
    outputFile << "Ay: "
        << static_cast<int>(accelerationsValue_5) << " "
        << static_cast<int>(accelerationsValue_6) << " "
        << static_cast<int>(accelerationsValue_7) << " "
        << static_cast<int>(accelerationsValue_8) << std::endl;
    outputFile << "Az: "
        << static_cast<int>(accelerationsValue_9) << " "
        << static_cast<int>(accelerationsValue_10) << " "
        << static_cast<int>(accelerationsValue_11) << " "
        << static_cast<int>(accelerationsValue_12) << std::endl;
    outputFile << "Bx: "
        << static_cast<int>(inductionValue_1) << " "
        << static_cast<int>(inductionValue_2) << " "
        << static_cast<int>(inductionValue_3) << " "
        << static_cast<int>(inductionValue_4) << std::endl;
    outputFile << "By: "
        << static_cast<int>(inductionValue_5) << " "
        << static_cast<int>(inductionValue_6) << " "
        << static_cast<int>(inductionValue_7) << " "
        << static_cast<int>(inductionValue_8) << std::endl;
    outputFile << "Bz: "
        << static_cast<int>(inductionValue_9) << " "
        << static_cast<int>(inductionValue_10) << " "
        << static_cast<int>(inductionValue_11) << " "
        << static_cast<int>(inductionValue_12) << std::endl;
    outputFile << "Value of magnetic field: "
        << static_cast<int>(magnitudeValue_1) << " "
        << static_cast<int>(magnitudeValue_2) << " "
        << static_cast<int>(magnitudeValue_3) << " "
        << static_cast<int>(magnitudeValue_4) << std::endl;
    outputFile << "Temperature of magnetic sensor: "
        << static_cast<int>(temperatureValue_1) << " "
        << static_cast<int>(temperatureValue_2) << " "
        << static_cast<int>(temperatureValue_3) << " "
        << static_cast<int>(temperatureValue_4) << std::endl;
    outputFile << "Calibrated value: " << static_cast<int>(calibratedValue) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var54::getCheckSum()
{
    return this->checkSum;
}

int Var54::getCsValue()
{
    return this->csValue;
}