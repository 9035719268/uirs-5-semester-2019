#include <iostream>
#include <fstream>
#include "../include/Var53.h"

int Var53::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x90257]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x90258]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x90259]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x9025A]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x9025B]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x9025C]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x9025D]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x9025E]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x9025F]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x90260]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x90261]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x90262]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x90263]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x90264]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x90265]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x90266]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x90267]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x90268]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x90269]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x9026A]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x9026B]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x9026C]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x9026D]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x9026E]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x9026F]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x90270]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x90271]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x90272]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x90273]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x90274]);

        this->accelerationsValue_1 = this->allValuesArr[5];
        this->accelerationsValue_2 = this->allValuesArr[6];
        this->accelerationsValue_3 = this->allValuesArr[7];
        this->accelerationsValue_4 = this->allValuesArr[8];
        this->accelerationsValue_5 = this->allValuesArr[9];
        this->accelerationsValue_6 = this->allValuesArr[10];
        this->accelerationsValue_7 = this->allValuesArr[11];
        this->accelerationsValue_8 = this->allValuesArr[12];
        this->accelerationsValue_9 = this->allValuesArr[13];
        this->accelerationsValue_10 = this->allValuesArr[14];
        this->accelerationsValue_11 = this->allValuesArr[15];
        this->accelerationsValue_12 = this->allValuesArr[16];
        this->angularVelocitiesValue_1 = this->allValuesArr[17];
        this->angularVelocitiesValue_2 = this->allValuesArr[18];
        this->angularVelocitiesValue_3 = this->allValuesArr[19];
        this->angularVelocitiesValue_4 = this->allValuesArr[20];
        this->angularVelocitiesValue_5 = this->allValuesArr[21];
        this->angularVelocitiesValue_6 = this->allValuesArr[22];
        this->angularVelocitiesValue_7 = this->allValuesArr[23];
        this->angularVelocitiesValue_8 = this->allValuesArr[24];
        this->angularVelocitiesValue_9 = this->allValuesArr[25];
        this->angularVelocitiesValue_10 = this->allValuesArr[26];
        this->angularVelocitiesValue_11 = this->allValuesArr[27];
        this->angularVelocitiesValue_12 = this->allValuesArr[28];
        this->csValue = this->allValuesArr[29];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var53::handleCS()
{
    this->checkSum = cs(allValuesArr, 29);
}

void Var53::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var53_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 53" << std::endl;
    outputFile << "Structure InertialMeasurements" << std::endl;
    outputFile << "Ax: "
        << static_cast<int>(accelerationsValue_1) << " "
        << static_cast<int>(accelerationsValue_2) << " "
        << static_cast<int>(accelerationsValue_3) << " "
        << static_cast<int>(accelerationsValue_4) << std::endl;
    outputFile << "Ay: "
        << static_cast<int>(accelerationsValue_5) << " "
        << static_cast<int>(accelerationsValue_6) << " "
        << static_cast<int>(accelerationsValue_7) << " "
        << static_cast<int>(accelerationsValue_8) << std::endl;
    outputFile << "Az: "
        << static_cast<int>(accelerationsValue_9) << " "
        << static_cast<int>(accelerationsValue_10) << " "
        << static_cast<int>(accelerationsValue_11) << " "
        << static_cast<int>(accelerationsValue_12) << std::endl;
    outputFile << "Wx: "
        << static_cast<int>(angularVelocitiesValue_1) << " "
        << static_cast<int>(angularVelocitiesValue_2) << " "
        << static_cast<int>(angularVelocitiesValue_3) << " "
        << static_cast<int>(angularVelocitiesValue_4) << std::endl;
    outputFile << "Wy: "
        << static_cast<int>(angularVelocitiesValue_5) << " "
        << static_cast<int>(angularVelocitiesValue_6) << " "
        << static_cast<int>(angularVelocitiesValue_7) << " "
        << static_cast<int>(angularVelocitiesValue_8) << std::endl;
    outputFile << "Wz: "
        << static_cast<int>(angularVelocitiesValue_9) << " "
        << static_cast<int>(angularVelocitiesValue_10) << " "
        << static_cast<int>(angularVelocitiesValue_11) << " "
        << static_cast<int>(angularVelocitiesValue_12) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var53::getCheckSum()
{
    return this->checkSum;
}

int Var53::getCsValue()
{
    return this->csValue;
}