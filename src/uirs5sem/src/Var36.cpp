#include <iostream>
#include <fstream>
#include "../include/Var36.h"

int Var36::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x191D]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x191E]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x191F]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x1920]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x1921]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x1922]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x1923]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x1924]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x1925]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x1926]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x1927]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x1928]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x1929]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x192A]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x192B]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x192C]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x192D]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x192E]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x192F]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x1930]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x1931]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x1932]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x1933]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x1934]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x1935]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x1936]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x1937]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x1938]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x1939]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x193A]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x193B]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x193C]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x193D]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x193E]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x193F]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x1940]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x1941]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x1942]);
        this->allValuesArr[38] = static_cast<u1>(tempString[0x1943]);
        this->allValuesArr[39] = static_cast<u1>(tempString[0x1944]);
        this->allValuesArr[40] = static_cast<u1>(tempString[0x1945]);
        this->allValuesArr[41] = static_cast<u1>(tempString[0x1946]);
        this->allValuesArr[42] = static_cast<u1>(tempString[0x1947]);
        this->allValuesArr[43] = static_cast<u1>(tempString[0x1948]);
        this->allValuesArr[44] = static_cast<u1>(tempString[0x1949]);
        this->allValuesArr[45] = static_cast<u1>(tempString[0x194A]);
        this->allValuesArr[46] = static_cast<u1>(tempString[0x194B]);
        this->allValuesArr[47] = static_cast<u1>(tempString[0x194C]);
        this->allValuesArr[48] = static_cast<u1>(tempString[0x194D]);
        this->allValuesArr[49] = static_cast<u1>(tempString[0x194E]);
        this->allValuesArr[50] = static_cast<u1>(tempString[0x194F]);
        this->allValuesArr[51] = static_cast<u1>(tempString[0x1950]);
        this->allValuesArr[52] = static_cast<u1>(tempString[0x1951]);
        this->allValuesArr[53] = static_cast<u1>(tempString[0x1952]);
        this->allValuesArr[54] = static_cast<u1>(tempString[0x1953]);
        this->allValuesArr[55] = static_cast<u1>(tempString[0x1954]);
        this->allValuesArr[56] = static_cast<u1>(tempString[0x1955]);

        this->bl0Value_1 = this->allValuesArr[5];
        this->bl0Value_2 = this->allValuesArr[6];
        this->bl0Value_3 = this->allValuesArr[7];
        this->bl0Value_4 = this->allValuesArr[8];
        this->bl0Value_5 = this->allValuesArr[9];
        this->bl0Value_6 = this->allValuesArr[10];
        this->bl0Value_7 = this->allValuesArr[11];
        this->bl0Value_8 = this->allValuesArr[12];
        this->bl0Value_9 = this->allValuesArr[13];
        this->bl0Value_10 = this->allValuesArr[14];
        this->bl0Value_11 = this->allValuesArr[15];
        this->bl0Value_12 = this->allValuesArr[16];
        this->bl1Value_1 = this->allValuesArr[17];
        this->bl1Value_2 = this->allValuesArr[18];
        this->bl1Value_3 = this->allValuesArr[19];
        this->bl1Value_4 = this->allValuesArr[20];
        this->bl1Value_5 = this->allValuesArr[21];
        this->bl1Value_6 = this->allValuesArr[22];
        this->bl1Value_7 = this->allValuesArr[23];
        this->bl1Value_8 = this->allValuesArr[24];
        this->bl1Value_9 = this->allValuesArr[25];
        this->bl1Value_10 = this->allValuesArr[26];
        this->bl1Value_11 = this->allValuesArr[27];
        this->bl1Value_12 = this->allValuesArr[28];
        this->bl2Value_1 = this->allValuesArr[29];
        this->bl2Value_2 = this->allValuesArr[30];
        this->bl2Value_3 = this->allValuesArr[31];
        this->bl2Value_4 = this->allValuesArr[32];
        this->bl2Value_5 = this->allValuesArr[33];
        this->bl2Value_6 = this->allValuesArr[34];
        this->bl2Value_7 = this->allValuesArr[35];
        this->bl2Value_8 = this->allValuesArr[36];
        this->bl2Value_9 = this->allValuesArr[37];
        this->bl2Value_10 = this->allValuesArr[38];
        this->bl2Value_11 = this->allValuesArr[39];
        this->bl2Value_12 = this->allValuesArr[40];
        this->rmsValue_1 = this->allValuesArr[41];
        this->rmsValue_2 = this->allValuesArr[42];
        this->rmsValue_3 = this->allValuesArr[43];
        this->rmsValue_4 = this->allValuesArr[44];
        this->rmsValue_5 = this->allValuesArr[45];
        this->rmsValue_6 = this->allValuesArr[46];
        this->rmsValue_7 = this->allValuesArr[47];
        this->rmsValue_8 = this->allValuesArr[48];
        this->rmsValue_9 = this->allValuesArr[49];
        this->rmsValue_10 = this->allValuesArr[50];
        this->rmsValue_11 = this->allValuesArr[51];
        this->rmsValue_12 = this->allValuesArr[52];
        this->solTypeValue_1 = this->allValuesArr[53];
        this->solTypeValue_2 = this->allValuesArr[54];
        this->solTypeValue_3 = this->allValuesArr[55];
        this->csValue = this->allValuesArr[56];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var36::handleCS()
{
    this->checkSum = cs(allValuesArr, 56);
}

void Var36::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var36_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 36" << std::endl;
    outputFile << "Structure Baselines" << std::endl;
    outputFile << "Baseline vector M-S0: "
        << static_cast<int>(bl0Value_1) << " "
        << static_cast<int>(bl0Value_2) << " "
        << static_cast<int>(bl0Value_3) << " "
        << static_cast<int>(bl0Value_4) << " "
        << static_cast<int>(bl0Value_5) << " "
        << static_cast<int>(bl0Value_6) << " "
        << static_cast<int>(bl0Value_7) << " "
        << static_cast<int>(bl0Value_8) << " "
        << static_cast<int>(bl0Value_9) << " "
        << static_cast<int>(bl0Value_10) << " "
        << static_cast<int>(bl0Value_11) << " "
        << static_cast<int>(bl0Value_12) << std::endl;
    outputFile << "Baseline vector M-S1: "
        << static_cast<int>(bl1Value_1) << " "
        << static_cast<int>(bl1Value_2) << " "
        << static_cast<int>(bl1Value_3) << " "
        << static_cast<int>(bl1Value_4) << " "
        << static_cast<int>(bl1Value_5) << " "
        << static_cast<int>(bl1Value_6) << " "
        << static_cast<int>(bl1Value_7) << " "
        << static_cast<int>(bl1Value_8) << " "
        << static_cast<int>(bl1Value_9) << " "
        << static_cast<int>(bl1Value_10) << " "
        << static_cast<int>(bl1Value_11) << " "
        << static_cast<int>(bl1Value_12) << std::endl;
    outputFile << "Baseline vector M-S2: "
        << static_cast<int>(bl2Value_1) << " "
        << static_cast<int>(bl2Value_2) << " "
        << static_cast<int>(bl2Value_3) << " "
        << static_cast<int>(bl2Value_4) << " "
        << static_cast<int>(bl2Value_5) << " "
        << static_cast<int>(bl2Value_6) << " "
        << static_cast<int>(bl2Value_7) << " "
        << static_cast<int>(bl2Value_8) << " "
        << static_cast<int>(bl2Value_9) << " "
        << static_cast<int>(bl2Value_10) << " "
        << static_cast<int>(bl2Value_11) << " "
        << static_cast<int>(bl2Value_12) << std::endl;
    outputFile << "Estimated accuracies for baseline vectors: "
        << static_cast<int>(rmsValue_1) << " "
        << static_cast<int>(rmsValue_2) << " "
        << static_cast<int>(rmsValue_3) << " "
        << static_cast<int>(rmsValue_4) << " "
        << static_cast<int>(rmsValue_5) << " "
        << static_cast<int>(rmsValue_6) << " "
        << static_cast<int>(rmsValue_7) << " "
        << static_cast<int>(rmsValue_8) << " "
        << static_cast<int>(rmsValue_9) << " "
        << static_cast<int>(rmsValue_10) << " "
        << static_cast<int>(rmsValue_11) << " "
        << static_cast<int>(rmsValue_12) << std::endl;
    outputFile << "Solution types for baseline vectors: "
        << static_cast<int>(solTypeValue_1) << " "
        << static_cast<int>(solTypeValue_2) << " "
        << static_cast<int>(solTypeValue_3) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var36::getCheckSum()
{
    return this->checkSum;
}

int Var36::getCsValue()
{
    return this->csValue;
}