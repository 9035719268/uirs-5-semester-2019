#include <iostream>
#include <fstream>
#include "../include/Var8.h"

int Var8::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x6F8]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x6F9]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x6FA]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x6FB]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x6FC]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x6FD]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x6FE]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x6FF]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x700]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x701]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x702]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x703]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x704]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x705]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x706]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x707]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x708]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x709]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x70A]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x70B]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x70C]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x70D]);

        this->valValue_1 = this->allValuesArr[5];
        this->valValue_2 = this->allValuesArr[6];
        this->valValue_3 = this->allValuesArr[7];
        this->valValue_4 = this->allValuesArr[8];
        this->valValue_5 = this->allValuesArr[9];
        this->valValue_6 = this->allValuesArr[10];
        this->valValue_7 = this->allValuesArr[11];
        this->valValue_8 = this->allValuesArr[12];
        this->svalValue_1 = this->allValuesArr[13];
        this->svalValue_2 = this->allValuesArr[14];
        this->svalValue_3 = this->allValuesArr[15];
        this->svalValue_4 = this->allValuesArr[16];
        this->svalValue_5 = this->allValuesArr[17];
        this->svalValue_6 = this->allValuesArr[18];
        this->svalValue_7 = this->allValuesArr[19];
        this->svalValue_8 = this->allValuesArr[20];
        this->csValue = this->allValuesArr[21];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var8::handleCS()
{
    this->checkSum = cs(allValuesArr, 21);
}

void Var8::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var8_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 8" << std::endl;
    outputFile << "Structure RcvGPSTimeOffset" << std::endl;
    outputFile << "(Tg - Tr): "
        << static_cast<int>(valValue_1) << " "
        << static_cast<int>(valValue_2) << " "
        << static_cast<int>(valValue_3) << " "
        << static_cast<int>(valValue_4) << " "
        << static_cast<int>(valValue_5) << " "
        << static_cast<int>(valValue_6) << " "
        << static_cast<int>(valValue_7) << " "
        << static_cast<int>(valValue_8) << std::endl;
    outputFile << "Smoothed (Tg - Tr): "
        << static_cast<int>(svalValue_1) << " "
        << static_cast<int>(svalValue_2) << " "
        << static_cast<int>(svalValue_3) << " "
        << static_cast<int>(svalValue_4) << " "
        << static_cast<int>(svalValue_5) << " "
        << static_cast<int>(svalValue_6) << " "
        << static_cast<int>(svalValue_7) << " "
        << static_cast<int>(svalValue_8) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var8::getCheckSum()
{
    return this->checkSum;
}

int Var8::getCsValue()
{
    return this->csValue;
}