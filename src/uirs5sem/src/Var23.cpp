#include <iostream>
#include <fstream>
#include "../include/Var23.h"

int Var23::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x1A20]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x1A21]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x1A22]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x1A23]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x1A24]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x1A25]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x1A26]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x1A27]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x1A28]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x1A29]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x1A2A]);

        this->timeValue_1 = this->allValuesArr[5];
        this->timeValue_2 = this->allValuesArr[6];
        this->timeValue_3 = this->allValuesArr[7];
        this->timeValue_4 = this->allValuesArr[8];
        this->solTypeValue = this->allValuesArr[9];
        this->csValue = this->allValuesArr[10];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var23::handleCS()
{
    this->checkSum = cs(allValuesArr, 10);
}

void Var23::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var23_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 23" << std::endl;
    outputFile << "Structure SolutionTime" << std::endl;
    outputFile << "Solution time: "
        << static_cast<int>(timeValue_1) << " "
        << static_cast<int>(timeValue_2) << " "
        << static_cast<int>(timeValue_3) << " "
        << static_cast<int>(timeValue_4) << std::endl;
    outputFile << "Solution type: " << static_cast<int>(solTypeValue) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var23::getCheckSum()
{
    return this->checkSum;
}

int Var23::getCsValue()
{
    return this->csValue;
}