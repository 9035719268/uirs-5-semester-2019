#include <iostream>
#include <fstream>
#include "../include/Var31.h"

int Var31::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x9345D]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x9345E]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x9345F]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x93460]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x93461]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x93462]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x93463]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x93464]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x93465]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x93466]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x93467]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x93468]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x93469]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x9346A]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x9346B]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x9346C]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x9346D]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x9346E]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x9346F]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x93470]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x93471]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x93472]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x93473]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x93474]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x93475]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x93476]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x93477]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x93478]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x93479]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x9347A]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x9347B]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x9347C]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x9347D]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x9347E]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x9347F]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x93480]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x93481]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x93482]);
        this->allValuesArr[38] = static_cast<u1>(tempString[0x93483]);
        this->allValuesArr[39] = static_cast<u1>(tempString[0x93484]);
        this->allValuesArr[40] = static_cast<u1>(tempString[0x93485]);
        this->allValuesArr[41] = static_cast<u1>(tempString[0x93486]);
        this->allValuesArr[42] = static_cast<u1>(tempString[0x93487]);
        this->allValuesArr[43] = static_cast<u1>(tempString[0x93488]);
        this->allValuesArr[44] = static_cast<u1>(tempString[0x93489]);
        this->allValuesArr[45] = static_cast<u1>(tempString[0x9348A]);
        this->allValuesArr[46] = static_cast<u1>(tempString[0x9348B]);

        this->nValue_1 = this->allValuesArr[5];
        this->nValue_2 = this->allValuesArr[6];
        this->nValue_3 = this->allValuesArr[7];
        this->nValue_4 = this->allValuesArr[8];
        this->nValue_5 = this->allValuesArr[9];
        this->nValue_6 = this->allValuesArr[10];
        this->nValue_7 = this->allValuesArr[11];
        this->nValue_8 = this->allValuesArr[12];
        this->eValue_1 = this->allValuesArr[13];
        this->eValue_2 = this->allValuesArr[14];
        this->eValue_3 = this->allValuesArr[15];
        this->eValue_4 = this->allValuesArr[16];
        this->eValue_5 = this->allValuesArr[17];
        this->eValue_6 = this->allValuesArr[18];
        this->eValue_7 = this->allValuesArr[19];
        this->eValue_8 = this->allValuesArr[20];
        this->uValue_1 = this->allValuesArr[21];
        this->uValue_2 = this->allValuesArr[22];
        this->uValue_3 = this->allValuesArr[23];
        this->uValue_4 = this->allValuesArr[24];
        this->uValue_5 = this->allValuesArr[25];
        this->uValue_6 = this->allValuesArr[26];
        this->uValue_7 = this->allValuesArr[27];
        this->uValue_8 = this->allValuesArr[28];
        this->sepValue_1 = this->allValuesArr[29];
        this->sepValue_2 = this->allValuesArr[30];
        this->sepValue_3 = this->allValuesArr[31];
        this->sepValue_4 = this->allValuesArr[32];
        this->sepValue_5 = this->allValuesArr[33];
        this->sepValue_6 = this->allValuesArr[34];
        this->sepValue_7 = this->allValuesArr[35];
        this->sepValue_8 = this->allValuesArr[36];
        this->pSigmaValue_1 = this->allValuesArr[37];
        this->pSigmaValue_2 = this->allValuesArr[38];
        this->pSigmaValue_3 = this->allValuesArr[39];
        this->pSigmaValue_4 = this->allValuesArr[40];
        this->solTypeValue = this->allValuesArr[41];
        this->gridValue = this->allValuesArr[42];
        this->geoidValue = this->allValuesArr[43];
        this->prjValue_1 = this->allValuesArr[44];
        this->prjValue_2 = this->allValuesArr[45];
        this->csValue = this->allValuesArr[46];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var31::handleCS()
{
    this->checkSum = cs(allValuesArr, 46);
}

void Var31::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var31_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 31" << std::endl;
    outputFile << "Structure RSLocalPlanePos" << std::endl;
    outputFile << "Northern coordinate: "
        << static_cast<int>(nValue_1) << " "
        << static_cast<int>(nValue_2) << " "
        << static_cast<int>(nValue_3) << " "
        << static_cast<int>(nValue_4) << " "
        << static_cast<int>(nValue_5) << " "
        << static_cast<int>(nValue_6) << " "
        << static_cast<int>(nValue_7) << " "
        << static_cast<int>(nValue_8) << std::endl;
    outputFile << "Eastern coordinate: "
        << static_cast<int>(eValue_1) << " "
        << static_cast<int>(eValue_2) << " "
        << static_cast<int>(eValue_3) << " "
        << static_cast<int>(eValue_4) << " "
        << static_cast<int>(eValue_5) << " "
        << static_cast<int>(eValue_6) << " "
        << static_cast<int>(eValue_7) << " "
        << static_cast<int>(eValue_8) << std::endl;
    outputFile << "Altitude above local ellipsoid: "
        << static_cast<int>(uValue_1) << " "
        << static_cast<int>(uValue_2) << " "
        << static_cast<int>(uValue_3) << " "
        << static_cast<int>(uValue_4) << " "
        << static_cast<int>(uValue_5) << " "
        << static_cast<int>(uValue_6) << " "
        << static_cast<int>(uValue_7) << " "
        << static_cast<int>(uValue_8) << std::endl;
    outputFile << "Geoid separation relatively to local ellipsoid: "
        << static_cast<int>(sepValue_1) << " "
        << static_cast<int>(sepValue_2) << " "
        << static_cast<int>(sepValue_3) << " "
        << static_cast<int>(sepValue_4) << " "
        << static_cast<int>(sepValue_5) << " "
        << static_cast<int>(sepValue_6) << " "
        << static_cast<int>(sepValue_7) << " "
        << static_cast<int>(sepValue_8) << std::endl;
    outputFile << "Position SEP: "
        << static_cast<int>(pSigmaValue_1) << " "
        << static_cast<int>(pSigmaValue_2) << " "
        << static_cast<int>(pSigmaValue_3) << " "
        << static_cast<int>(pSigmaValue_4) << std::endl;
    outputFile << "Solution type: " << static_cast<int>(solTypeValue) << std::endl;
    outputFile << "Grid source: " << static_cast<int>(gridValue) << std::endl;
    outputFile << "Geoid source: " << static_cast<int>(geoidValue) << std::endl;
    outputFile << "EPSG code of used projection: "
        << static_cast<int>(prjValue_1) << " "
        << static_cast<int>(prjValue_2) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var31::getCheckSum()
{
    return this->checkSum;
}

int Var31::getCsValue()
{
    return this->csValue;
}