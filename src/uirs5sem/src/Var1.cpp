#include <iostream>
#include <fstream>
#include "../include/Var1.h"

int Var1::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x224]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x225]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x226]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x227]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x228]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x229]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x22A]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x22B]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x22C]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x22D]);

        this->todValue_1 = this->allValuesArr[5];
        this->todValue_2 = this->allValuesArr[6];
        this->todValue_3 = this->allValuesArr[7];
        this->todValue_4 = this->allValuesArr[8];
        this->csValue = this->allValuesArr[9];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var1::handleCS()
{
    this->checkSum = cs(allValuesArr, 9);
}

void Var1::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var1_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 1" << std::endl;
    outputFile << "Structure RcvTime" << std::endl;
    outputFile << "Tr modulo 1 day: "
        << static_cast<int>(todValue_1) << " "
        << static_cast<int>(todValue_2) << " "
        << static_cast<int>(todValue_3) << " "
        << static_cast<int>(todValue_4) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var1::getCheckSum()
{
    return this->checkSum;
}

int Var1::getCsValue()
{
    return this->csValue;
}