#include <iostream>
#include <fstream>
#include "../include/Var67.h"

int Var67::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x3C1C7]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x3C1C8]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x3C1C9]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x3C1CA]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x3C1CB]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x3C1CC]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x3C1CD]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x3C1CE]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x3C1CF]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x3C1D0]);

        this->valValue_1 = this->allValuesArr[5];
        this->valValue_2 = this->allValuesArr[6];
        this->valValue_3 = this->allValuesArr[7];
        this->valValue_4 = this->allValuesArr[8];
        this->csValue = this->allValuesArr[9];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var67::handleCS()
{
    this->checkSum = cs(allValuesArr, 9);
}

void Var67::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var67_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 67" << std::endl;
    outputFile << "Structure RcvOscOffs" << std::endl;
    outputFile << "Oscillator offset: "
        << static_cast<int>(valValue_1) << " "
        << static_cast<int>(valValue_2) << " "
        << static_cast<int>(valValue_3) << " "
        << static_cast<int>(valValue_4) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var67::getCheckSum()
{
    return this->checkSum;
}

int Var67::getCsValue()
{
    return this->csValue;
}