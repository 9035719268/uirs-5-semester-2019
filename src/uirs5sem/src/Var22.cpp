#include <iostream>
#include <fstream>
#include "../include/Var22.h"

int Var22::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x768]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x769]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x76A]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x76B]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x76C]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x76D]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x76E]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x76F]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x770]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x771]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x772]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x773]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x774]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x775]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x776]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x777]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x778]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x779]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x77A]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x77B]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x77C]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x77D]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x77E]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x77F]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x780]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x781]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x782]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x783]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x784]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x785]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x786]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x787]);

        this->tauSysValue_1 = this->allValuesArr[5];
        this->tauSysValue_2 = this->allValuesArr[6];
        this->tauSysValue_3 = this->allValuesArr[7];
        this->tauSysValue_4 = this->allValuesArr[8];
        this->tauSysValue_5 = this->allValuesArr[9];
        this->tauSysValue_6 = this->allValuesArr[10];
        this->tauSysValue_7 = this->allValuesArr[11];
        this->tauSysValue_8 = this->allValuesArr[12];
        this->tauGpsValue_1 = this->allValuesArr[13];
        this->tauGpsValue_2 = this->allValuesArr[14];
        this->tauGpsValue_3 = this->allValuesArr[15];
        this->tauGpsValue_4 = this->allValuesArr[16];
        this->B1Value_1 = this->allValuesArr[17];
        this->B1Value_2 = this->allValuesArr[18];
        this->B1Value_3 = this->allValuesArr[19];
        this->B1Value_4 = this->allValuesArr[20];
        this->B2Value_1 = this->allValuesArr[21];
        this->B2Value_2 = this->allValuesArr[22];
        this->B2Value_3 = this->allValuesArr[23];
        this->B2Value_4 = this->allValuesArr[24];
        this->KPValue = this->allValuesArr[25];
        this->N4Value = this->allValuesArr[26];
        this->DnValue_1 = this->allValuesArr[27];
        this->DnValue_2 = this->allValuesArr[28];
        this->NtValue_1 = this->allValuesArr[29];
        this->NtValue_2 = this->allValuesArr[30];
        this->csValue = this->allValuesArr[31];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var22::handleCS()
{
    this->checkSum = cs(allValuesArr, 31);
}

void Var22::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var22_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 22" << std::endl;
    outputFile << "Structure GloUtcGpsParam" << std::endl;
    outputFile << "Time correction to GLONASS time scale: "
        << static_cast<int>(tauSysValue_1) << " "
        << static_cast<int>(tauSysValue_2) << " "
        << static_cast<int>(tauSysValue_3) << " "
        << static_cast<int>(tauSysValue_4) << " "
        << static_cast<int>(tauSysValue_5) << " "
        << static_cast<int>(tauSysValue_6) << " "
        << static_cast<int>(tauSysValue_7) << " "
        << static_cast<int>(tauSysValue_8) << std::endl;
    outputFile << "(Tgps - Tglo): "
        << static_cast<int>(tauGpsValue_1) << " "
        << static_cast<int>(tauGpsValue_2) << " "
        << static_cast<int>(tauGpsValue_3) << " "
        << static_cast<int>(tauGpsValue_4) << std::endl;
    outputFile << "Coefficient B1 for calculation of UT1: "
        << static_cast<int>(B1Value_1) << " "
        << static_cast<int>(B1Value_2) << " "
        << static_cast<int>(B1Value_3) << " "
        << static_cast<int>(B1Value_4) << std::endl;
    outputFile << "Coefficient B2 for calculation of UT1: "
        << static_cast<int>(B2Value_1) << " "
        << static_cast<int>(B2Value_2) << " "
        << static_cast<int>(B2Value_3) << " "
        << static_cast<int>(B2Value_4) << std::endl;
    outputFile << "Leap second information: " << static_cast<int>(KPValue) << std::endl;
    outputFile << "Number of 4-year cycle: " << static_cast<int>(N4Value) << std::endl;
    outputFile << "Day number within 4-year period: "
        << static_cast<int>(DnValue_1) << " "
        << static_cast<int>(DnValue_2) << std::endl;
    outputFile << "Current day number at the decoding time: "
        << static_cast<int>(NtValue_1) << " "
        << static_cast<int>(NtValue_2) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var22::getCheckSum()
{
    return this->checkSum;
}

int Var22::getCsValue()
{
    return this->csValue;
}