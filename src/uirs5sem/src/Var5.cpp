#include <iostream>
#include <fstream>
#include "../include/Var5.h"

int Var5::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x89]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x8A]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x8B]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x8C]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x8D]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x8E]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x8F]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x90]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x91]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x92]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x93]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x94]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x95]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x96]);

        this->valValue_1 = this->allValuesArr[5];
        this->valValue_2 = this->allValuesArr[6];
        this->valValue_3 = this->allValuesArr[7];
        this->valValue_4 = this->allValuesArr[8];
        this->svalValue_1 = this->allValuesArr[9];
        this->svalValue_2 = this->allValuesArr[10];
        this->svalValue_3 = this->allValuesArr[11];
        this->svalValue_4 = this->allValuesArr[12];
        this->csValue = this->allValuesArr[13];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var5::handleCS()
{
    this->checkSum = cs(allValuesArr, 13);
}

void Var5::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var5_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 5" << std::endl;
    outputFile << "Structure RcvTimeOffsetDot" << std::endl;
    outputFile << "Derivative of (Trr - Tr): "
        << static_cast<int>(valValue_1) << " "
        << static_cast<int>(valValue_2) << " "
        << static_cast<int>(valValue_3) << " "
        << static_cast<int>(valValue_4) << std::endl;
    outputFile << "Smoothed derivative of (Trr - Tr): "
        << static_cast<int>(svalValue_1) << " "
        << static_cast<int>(svalValue_2) << " "
        << static_cast<int>(svalValue_3) << " "
        << static_cast<int>(svalValue_4) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var5::getCheckSum()
{
    return this->checkSum;
}

int Var5::getCsValue()
{
    return this->csValue;
}