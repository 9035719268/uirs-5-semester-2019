#include <iostream>
#include <fstream>
#include "../include/Var68.h"

int Var68::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x1957]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x1958]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x1959]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x195A]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x195B]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x195C]);

        this->csValue = this->allValuesArr[5];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var68::handleCS()
{
    this->checkSum = cs(allValuesArr, 5);
}

void Var68::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var68_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 68" << std::endl;
    outputFile << "Structure EpochEnd" << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var68::getCheckSum()
{
    return this->checkSum;
}

int Var68::getCsValue()
{
    return this->csValue;
}