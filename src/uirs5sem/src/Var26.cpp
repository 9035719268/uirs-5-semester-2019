#include <iostream>
#include <fstream>
#include "../include/Var26.h"

int Var26::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x2F0E6]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x2F0E7]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x2F0E8]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x2F0E9]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x2F0EA]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x2F0EB]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x2F0EC]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x2F0ED]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x2F0EE]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x2F0EF]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x2F0F0]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x2F0F1]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x2F0F2]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x2F0F3]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x2F0F4]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x2F0F5]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x2F0F6]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x2F0F7]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x2F0F8]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x2F0F9]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x2F0FA]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x2F0FB]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x2F0FC]);

        this->xValue_1 = this->allValuesArr[5];
        this->xValue_2 = this->allValuesArr[6];
        this->xValue_3 = this->allValuesArr[7];
        this->xValue_4 = this->allValuesArr[8];
        this->yValue_1 = this->allValuesArr[9];
        this->yValue_2 = this->allValuesArr[10];
        this->yValue_3 = this->allValuesArr[11];
        this->yValue_4 = this->allValuesArr[12];
        this->zValue_1 = this->allValuesArr[13];
        this->zValue_2 = this->allValuesArr[14];
        this->zValue_3 = this->allValuesArr[15];
        this->zValue_4 = this->allValuesArr[16];
        this->vSigmaValue_1 = this->allValuesArr[17];
        this->vSigmaValue_2 = this->allValuesArr[18];
        this->vSigmaValue_3 = this->allValuesArr[19];
        this->vSigmaValue_4 = this->allValuesArr[20];
        this->solTypeValue = this->allValuesArr[21];
        this->csValue = this->allValuesArr[22];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var26::handleCS()
{
    this->checkSum = cs(allValuesArr, 22);
}

void Var26::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var26_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 26" << std::endl;
    outputFile << "Structure Vel" << std::endl;
    outputFile << "Cartesian velocity vector:" << std::endl;
    outputFile << "x: "
        << static_cast<int>(xValue_1) << " "
        << static_cast<int>(xValue_2) << " "
        << static_cast<int>(xValue_3) << " "
        << static_cast<int>(xValue_4) << std::endl;
    outputFile << "y: "
        << static_cast<int>(yValue_1) << " "
        << static_cast<int>(yValue_2) << " "
        << static_cast<int>(yValue_3) << " "
        << static_cast<int>(yValue_4) << std::endl;
    outputFile << "z: "
        << static_cast<int>(zValue_1) << " "
        << static_cast<int>(zValue_2) << " "
        << static_cast<int>(zValue_3) << " "
        << static_cast<int>(zValue_4) << std::endl;
    outputFile << "Velocity SEP: "
        << static_cast<int>(vSigmaValue_1) << " "
        << static_cast<int>(vSigmaValue_2) << " "
        << static_cast<int>(vSigmaValue_3) << " "
        << static_cast<int>(vSigmaValue_4) << std::endl;
    outputFile << "Solution type: " << static_cast<int>(solTypeValue) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var26::getCheckSum()
{
    return this->checkSum;
}

int Var26::getCsValue()
{
    return this->csValue;
}