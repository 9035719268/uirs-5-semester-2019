#include <iostream>
#include <fstream>
#include "../include/Var58.h"

int Var58::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x41C9]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x41CA]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x41CB]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x41CC]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x41CD]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x41CE]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x41CF]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x41D0]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x41D1]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x41D2]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x41D3]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x41D4]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x41D5]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x41D6]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x41D7]);

        this->headingValue_1 = this->allValuesArr[5];
        this->headingValue_2 = this->allValuesArr[6];
        this->headingValue_3 = this->allValuesArr[7];
        this->headingValue_4 = this->allValuesArr[8];
        this->pitchValue_1 = this->allValuesArr[9];
        this->pitchValue_2 = this->allValuesArr[10];
        this->pitchValue_3 = this->allValuesArr[11];
        this->pitchValue_4 = this->allValuesArr[12];
        this->solTypeValue = this->allValuesArr[13];
        this->csValue = this->allValuesArr[14];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var58::handleCS()
{
    this->checkSum = cs(allValuesArr, 14);
}

void Var58::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var58_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 58" << std::endl;
    outputFile << "Structure HeadAndPitch" << std::endl;
    outputFile << "Heading of the baseline between the base and the rover receiver: "
        << static_cast<int>(headingValue_1) << " "
        << static_cast<int>(headingValue_2) << " "
        << static_cast<int>(headingValue_3) << " "
        << static_cast<int>(headingValue_4) << std::endl;
    outputFile << "Pitch of the baseline between the base and the rover receiver: "
        << static_cast<int>(pitchValue_1) << " "
        << static_cast<int>(pitchValue_2) << " "
        << static_cast<int>(pitchValue_3) << " "
        << static_cast<int>(pitchValue_4) << std::endl;
    outputFile << "Solution type: " << static_cast<int>(solTypeValue) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var58::getCheckSum()
{
    return this->checkSum;
}

int Var58::getCsValue()
{
    return this->csValue;
}