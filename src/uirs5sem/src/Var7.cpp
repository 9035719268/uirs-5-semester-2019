#include <iostream>
#include <fstream>
#include "../include/Var7.h"

int Var7::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x6EB]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x6EC]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x6ED]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x6EE]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x6EF]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x6F0]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x6F1]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x6F2]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x6F3]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x6F4]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x6F5]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x6F6]);

        this->towValue_1 = this->allValuesArr[5];
        this->towValue_2 = this->allValuesArr[6];
        this->towValue_3 = this->allValuesArr[7];
        this->towValue_4 = this->allValuesArr[8];
        this->wnValue_1 = this->allValuesArr[9];
        this->wnValue_2 = this->allValuesArr[10];
        this->csValue = this->allValuesArr[11];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var7::handleCS()
{
    this->checkSum = cs(allValuesArr, 11);
}

void Var7::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var7_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 7" << std::endl;
    outputFile << "Structure GPSTime" << std::endl;
    outputFile << "Time of week: "
        << static_cast<int>(towValue_1) << " "
        << static_cast<int>(towValue_2) << " "
        << static_cast<int>(towValue_3) << " "
        << static_cast<int>(towValue_4) << std::endl;
    outputFile << "GPS week number: "
        << static_cast<int>(wnValue_1) << " "
        << static_cast<int>(wnValue_2) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var7::getCheckSum()
{
    return this->checkSum;
}

int Var7::getCsValue()
{
    return this->csValue;
}