#include <iostream>
#include <fstream>
#include "../include/Var62.h"

int Var62::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x22404]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x22405]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x22406]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x22407]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x22408]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x22409]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x2240A]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x2240B]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x2240C]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x2240D]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x2240E]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x2240F]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x22410]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x22411]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x22412]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x22413]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x22414]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x22415]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x22416]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x22417]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x22418]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x22419]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x2241A]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x2241B]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x2241C]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x2241D]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x2241E]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x2241F]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x22420]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x22421]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x22422]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x22423]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x22424]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x22425]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x22426]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x22427]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x22428]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x22429]);
        this->allValuesArr[38] = static_cast<u1>(tempString[0x2242A]);
        this->allValuesArr[39] = static_cast<u1>(tempString[0x2242B]);
        this->allValuesArr[40] = static_cast<u1>(tempString[0x2242C]);
        this->allValuesArr[41] = static_cast<u1>(tempString[0x2242D]);
        this->allValuesArr[42] = static_cast<u1>(tempString[0x2242E]);
        this->allValuesArr[43] = static_cast<u1>(tempString[0x2242F]);

        this->totValue_1 = this->allValuesArr[5];
        this->totValue_2 = this->allValuesArr[6];
        this->totValue_3 = this->allValuesArr[7];
        this->totValue_4 = this->allValuesArr[8];
        this->wnValue_1 = this->allValuesArr[9];
        this->wnValue_2 = this->allValuesArr[10];
        this->alpha0Value_1 = this->allValuesArr[11];
        this->alpha0Value_2 = this->allValuesArr[12];
        this->alpha0Value_3 = this->allValuesArr[13];
        this->alpha0Value_4 = this->allValuesArr[14];
        this->alpha1Value_1 = this->allValuesArr[15];
        this->alpha1Value_2 = this->allValuesArr[16];
        this->alpha1Value_3 = this->allValuesArr[17];
        this->alpha1Value_4 = this->allValuesArr[18];
        this->alpha2Value_1 = this->allValuesArr[19];
        this->alpha2Value_2 = this->allValuesArr[20];
        this->alpha2Value_3 = this->allValuesArr[21];
        this->alpha2Value_4 = this->allValuesArr[22];
        this->alpha3Value_1 = this->allValuesArr[23];
        this->alpha3Value_2 = this->allValuesArr[24];
        this->alpha3Value_3 = this->allValuesArr[25];
        this->alpha3Value_4 = this->allValuesArr[26];
        this->beta0Value_1 = this->allValuesArr[27];
        this->beta0Value_2 = this->allValuesArr[28];
        this->beta0Value_3 = this->allValuesArr[29];
        this->beta0Value_4 = this->allValuesArr[30];
        this->beta1Value_1 = this->allValuesArr[31];
        this->beta1Value_2 = this->allValuesArr[32];
        this->beta1Value_3 = this->allValuesArr[33];
        this->beta1Value_4 = this->allValuesArr[34];
        this->beta2Value_1 = this->allValuesArr[35];
        this->beta2Value_2 = this->allValuesArr[36];
        this->beta2Value_3 = this->allValuesArr[37];
        this->beta2Value_4 = this->allValuesArr[38];
        this->beta3Value_1 = this->allValuesArr[39];
        this->beta3Value_2 = this->allValuesArr[40];
        this->beta3Value_3 = this->allValuesArr[41];
        this->beta3Value_4 = this->allValuesArr[42];
        this->csValue = this->allValuesArr[43];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var62::handleCS()
{
    this->checkSum = cs(allValuesArr, 43);
}

void Var62::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var62_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 62" << std::endl;
    outputFile << "Structure IrnssIonoParams" << std::endl;
    outputFile << "Time of week: "
        << static_cast<int>(totValue_1) << " "
        << static_cast<int>(totValue_2) << " "
        << static_cast<int>(totValue_3) << " "
        << static_cast<int>(totValue_4) << std::endl;
    outputFile << "Week number: "
        << static_cast<int>(wnValue_1) << " "
        << static_cast<int>(wnValue_2) << std::endl;
    outputFile << "The coefficients of a cubic equation representing the amplitude of the vertical delay: "
        << static_cast<int>(alpha0Value_1) << " "
        << static_cast<int>(alpha0Value_2) << " "
        << static_cast<int>(alpha0Value_3) << " "
        << static_cast<int>(alpha0Value_4) << " "
        << static_cast<int>(alpha1Value_1) << " "
        << static_cast<int>(alpha1Value_2) << " "
        << static_cast<int>(alpha1Value_3) << " "
        << static_cast<int>(alpha1Value_4) << " "
        << static_cast<int>(alpha2Value_1) << " "
        << static_cast<int>(alpha2Value_2) << " "
        << static_cast<int>(alpha2Value_3) << " "
        << static_cast<int>(alpha2Value_4) << " "
        << static_cast<int>(alpha3Value_1) << " "
        << static_cast<int>(alpha3Value_2) << " "
        << static_cast<int>(alpha3Value_3) << " "
        << static_cast<int>(alpha3Value_4) << std::endl;
    outputFile << "The coefficients of a cubic equation representing the period of the model: "
        << static_cast<int>(beta0Value_1) << " "
        << static_cast<int>(beta0Value_2) << " "
        << static_cast<int>(beta0Value_3) << " "
        << static_cast<int>(beta0Value_4) << " "
        << static_cast<int>(beta1Value_1) << " "
        << static_cast<int>(beta1Value_2) << " "
        << static_cast<int>(beta1Value_3) << " "
        << static_cast<int>(beta1Value_4) << " "
        << static_cast<int>(beta2Value_1) << " "
        << static_cast<int>(beta2Value_2) << " "
        << static_cast<int>(beta2Value_3) << " "
        << static_cast<int>(beta2Value_4) << " "
        << static_cast<int>(beta3Value_1) << " "
        << static_cast<int>(beta3Value_2) << " "
        << static_cast<int>(beta3Value_3) << " "
        << static_cast<int>(beta3Value_4) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var62::getCheckSum()
{
    return this->checkSum;
}

int Var62::getCsValue()
{
    return this->csValue;
}