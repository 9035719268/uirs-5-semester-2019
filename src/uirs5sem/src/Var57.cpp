#include <iostream>
#include <fstream>
#include "../include/Var57.h"

int Var57::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x893]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x894]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x895]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x896]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x897]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x898]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x899]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x89A]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x89B]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x89C]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x89D]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x89E]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x89F]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x8A0]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x8A1]);

        this->offsValue_1 = this->allValuesArr[5];
        this->offsValue_2 = this->allValuesArr[6];
        this->offsValue_3 = this->allValuesArr[7];
        this->offsValue_4 = this->allValuesArr[8];
        this->offsValue_5 = this->allValuesArr[9];
        this->offsValue_6 = this->allValuesArr[10];
        this->offsValue_7 = this->allValuesArr[11];
        this->offsValue_8 = this->allValuesArr[12];
        this->timeScaleValue = this->allValuesArr[13];
        this->csValue = this->allValuesArr[14];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var57::handleCS()
{
    this->checkSum = cs(allValuesArr, 14);
}

void Var57::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var57_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 57" << std::endl;
    outputFile << "Structure RcvTimeOffsAtPPS" << std::endl;
    outputFile << "Offset: "
        << static_cast<int>(offsValue_1) << " "
        << static_cast<int>(offsValue_2) << " "
        << static_cast<int>(offsValue_3) << " "
        << static_cast<int>(offsValue_4) << " "
        << static_cast<int>(offsValue_5) << " "
        << static_cast<int>(offsValue_6) << " "
        << static_cast<int>(offsValue_7) << " "
        << static_cast<int>(offsValue_8) << std::endl;
    outputFile << "Time scale: " << static_cast<int>(timeScaleValue) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var57::getCheckSum()
{
    return this->checkSum;
}

int Var57::getCsValue()
{
    return this->csValue;
}