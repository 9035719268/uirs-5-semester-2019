#include <iostream>
#include <fstream>
#include "../include/Var48.h"

int Var48::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x4AD5]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x4AD6]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x4AD7]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x4AD8]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x4AD9]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x4ADA]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x4ADB]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x4ADC]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x4ADD]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x4ADE]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x4ADF]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x4AE0]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x4AE1]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x4AE2]);

        this->frqValue_1 = this->allValuesArr[5];
        this->frqValue_2 = this->allValuesArr[6];
        this->frqValue_3 = this->allValuesArr[7];
        this->frqValue_4 = this->allValuesArr[8];
        this->pwrValue_1 = this->allValuesArr[9];
        this->pwrValue_2 = this->allValuesArr[10];
        this->pwrValue_3 = this->allValuesArr[11];
        this->pwrValue_4 = this->allValuesArr[12];
        this->csValue = this->allValuesArr[13];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var48::handleCS()
{
    this->checkSum = cs(allValuesArr, 13);
}

void Var48::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var48_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 48" << std::endl;
    outputFile << "Structure MDM_Spectrum" << std::endl;
    outputFile << "Current frequency: "
        << static_cast<int>(frqValue_1) << " "
        << static_cast<int>(frqValue_2) << " "
        << static_cast<int>(frqValue_3) << " "
        << static_cast<int>(frqValue_4) << std::endl;
    outputFile << "Current signal (or noise) power: "
        << static_cast<int>(pwrValue_1) << " "
        << static_cast<int>(pwrValue_2) << " "
        << static_cast<int>(pwrValue_3) << " "
        << static_cast<int>(pwrValue_4) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var48::getCheckSum()
{
    return this->checkSum;
}

int Var48::getCsValue()
{
    return this->csValue;
}