#include "pch.h"
#include "../../uirs5sem/include/Var9.h"
#include "../../uirs5sem/src/Var9.cpp"

TEST(Var9Test, isCheckSumOK)
{
	Var9 var9;
	int checksum = var9.getCheckSum();
	int csvalue = var9.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}