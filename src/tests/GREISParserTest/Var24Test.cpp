#include "pch.h"
#include "../../uirs5sem/include/Var24.h"
#include "../../uirs5sem/src/Var24.cpp"

TEST(Var24Test, isCheckSumOK)
{
	Var24 var24;
	int checksum = var24.getCheckSum();
	int csvalue = var24.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}