#include "pch.h"
#include "../../uirs5sem/include/Var1.h"
#include "../../uirs5sem/src/Var1.cpp"

TEST(Var1Test, isCheckSumOK)
{
	Var1 var1;
	int checksum = var1.getCheckSum();
	int csvalue = var1.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}