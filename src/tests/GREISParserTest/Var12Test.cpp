#include "pch.h"
#include "../../uirs5sem/include/Var12.h"
#include "../../uirs5sem/src/Var12.cpp"

TEST(Var12Test, isCheckSumOK)
{
	Var12 var12;
	int checksum = var12.getCheckSum();
	int csvalue = var12.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}