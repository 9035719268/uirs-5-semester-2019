#include "pch.h"
#include "../../uirs5sem/include/Var53.h"
#include "../../uirs5sem/src/Var53.cpp"

TEST(Var53Test, isCheckSumOK)
{
	Var53 var53;
	int checksum = var53.getCheckSum();
	int csvalue = var53.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}