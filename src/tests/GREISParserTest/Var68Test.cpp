#include "pch.h"
#include "../../uirs5sem/include/Var68.h"
#include "../../uirs5sem/src/Var68.cpp"

TEST(Var68Test, isCheckSumOK)
{
	Var68 var68;
	int checksum = var68.getCheckSum();
	int csvalue = var68.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}