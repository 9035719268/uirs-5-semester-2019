#include "pch.h"
#include "../../uirs5sem/include/Var57.h"
#include "../../uirs5sem/src/Var57.cpp"

TEST(Var57Test, isCheckSumOK)
{
	Var57 var57;
	int checksum = var57.getCheckSum();
	int csvalue = var57.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}