#include "pch.h"
#include "../../uirs5sem/include/Var52.h"
#include "../../uirs5sem/src/Var52.cpp"

TEST(Var52Test, isCheckSumOK)
{
	Var52 var52;
	int checksum = var52.getCheckSum();
	int csvalue = var52.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}