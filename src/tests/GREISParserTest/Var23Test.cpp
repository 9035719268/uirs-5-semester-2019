#include "pch.h"
#include "../../uirs5sem/include/Var23.h"
#include "../../uirs5sem/src/Var23.cpp"

TEST(Var23Test, isCheckSumOK)
{
	Var23 var23;
	int checksum = var23.getCheckSum();
	int csvalue = var23.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}