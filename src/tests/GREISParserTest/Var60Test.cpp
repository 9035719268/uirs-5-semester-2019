#include "pch.h"
#include "../../uirs5sem/include/Var60.h"
#include "../../uirs5sem/src/Var60.cpp"

TEST(Var60Test, isCheckSumOK)
{
	Var60 var60;
	int checksum = var60.getCheckSum();
	int csvalue = var60.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}