#include "pch.h"
#include "../../uirs5sem/include/Var45.h"
#include "../../uirs5sem/src/Var45.cpp"

TEST(Var45Test, isCheckSumOK)
{
	Var45 var45;
	int checksum = var45.getCheckSum();
	int csvalue = var45.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}