#include "pch.h"
#include "../../uirs5sem/include/Var34.h"
#include "../../uirs5sem/src/Var34.cpp"

TEST(Var34Test, isCheckSumOK)
{
	Var34 var34;
	int checksum = var34.getCheckSum();
	int csvalue = var34.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}