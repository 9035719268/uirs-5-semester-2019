#include "pch.h"
#include "../../uirs5sem/include/Var64.h"
#include "../../uirs5sem/src/Var64.cpp"

TEST(Var64Test, isCheckSumOK)
{
	Var64 var64;
	int checksum = var64.getCheckSum();
	int csvalue = var64.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}