#include "pch.h"
#include "../../uirs5sem/include/Var19.h"
#include "../../uirs5sem/src/Var19.cpp"

TEST(Var19Test, isCheckSumOK)
{
	Var19 var19;
	int checksum = var19.getCheckSum();
	int csvalue = var19.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}