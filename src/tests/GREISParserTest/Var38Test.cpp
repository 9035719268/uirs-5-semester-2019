#include "pch.h"
#include "../../uirs5sem/include/Var38.h"
#include "../../uirs5sem/src/Var38.cpp"

TEST(Var38Test, isCheckSumOK)
{
	Var38 var38;
	int checksum = var38.getCheckSum();
	int csvalue = var38.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}