#include "pch.h"
#include "../../uirs5sem/include/Var14.h"
#include "../../uirs5sem/src/Var14.cpp"

TEST(Var14Test, isCheckSumOK)
{
	Var14 var14;
	int checksum = var14.getCheckSum();
	int csvalue = var14.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}