#include "pch.h"
#include "../../uirs5sem/include/Var37.h"
#include "../../uirs5sem/src/Var37.cpp"

TEST(Var37Test, isCheckSumOK)
{
	Var37 var37;
	int checksum = var37.getCheckSum();
	int csvalue = var37.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}