#include "pch.h"
#include "../../uirs5sem/include/Var22.h"
#include "../../uirs5sem/src/Var22.cpp"

TEST(Var22Test, isCheckSumOK)
{
	Var22 var22;
	int checksum = var22.getCheckSum();
	int csvalue = var22.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}