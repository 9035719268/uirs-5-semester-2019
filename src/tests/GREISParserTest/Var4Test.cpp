#include "pch.h"
#include "../../uirs5sem/include/Var4.h"
#include "../../uirs5sem/src/Var4.cpp"

TEST(Var4Test, isCheckSumOK)
{
	Var4 var4;
	int checksum = var4.getCheckSum();
	int csvalue = var4.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}